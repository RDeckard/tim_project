# frozen_string_literal: true

module TimProject
  module Models
    class Tim < BasicTile
      include Singleton

      SPEED = Window::GRID_TILE_SIZE * 8196 * 3
      ANIMATION_SPEED = 250

      def initialize
        @images = Assets::TIM_SPRITES
        @zorder = ZOrders::MID
        @body = CP::Body.new(50, Float::INFINITY)
        @body.pos = Window::GRID[4][4]
        @shape =
          CP::Shape::Circle.new(@body, COLLISION_RADIUS, CP::Vec2.new(0, 0))
        @shape.layers = 1 << @zorder
        @animation_speed = ANIMATION_SPEED
      end

      def go_up
        @direction = :walk_up
        move CP::Vec2.new(0, -SPEED)
      end

      def go_down
        @direction = :walk_down
        move CP::Vec2.new(0, SPEED)
      end

      def go_left
        @direction = :walk_left
        move CP::Vec2.new(-SPEED, 0)
      end

      def go_right
        @direction = :walk_right
        move CP::Vec2.new(SPEED, 0)
      end

      def draw
        super(@images[@direction || :idle])
      end

      private

      def move(vec)
        body.apply_force(vec, CP::Vec2.new(0, 0))
      end
    end
  end
end
