# frozen_string_literal: true

require_relative 'basic_tile'
require_relative 'tile'
require_relative 'tiles_grid'
require_relative 'tim'
