# frozen_string_literal: true

module TimProject
  module Models
    class BasicTile
      COLLISION_RADIUS = Window::GRID_TILE_SIZE / 2

      attr_reader :body

      # CP::Space#add_object use this method on the object passed in parameter
      def chipmunk_objects
        [@body, @shape]
      end

      private

      def draw(images)
        animate(images).
          draw(
            @body.pos.x - Window::GRID_TILE_SIZE / 2,
            @body.pos.y - Window::GRID_TILE_SIZE / 2,
            @zorder,
            Assets::SCALE, Assets::SCALE
          )
      end

      def animate(images)
        images[Gosu.milliseconds / @animation_speed % images.size]
      end
    end
  end
end
