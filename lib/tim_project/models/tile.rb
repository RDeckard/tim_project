# frozen_string_literal: true

module TimProject
  module Models
    class Tile < BasicTile
      def initialize(*images, type: nil, pos:, zorder: ZOrders::GROUND, animation_speed: 500)
        @images = images.flatten
        @zorder = zorder
        @type = type
        @body = CP::BodyStatic.new
        @body.pos = pos
        @shape = CP::Shape::Circle.new(@body, COLLISION_RADIUS, CP::Vec2.new(0, 0))
        @shape.layers = 1 << @zorder
        @shape.layers |= (1 << ZOrders::MID) if block?
        @animation_speed = animation_speed
      end

      def draw
        super(@images)
      end

      def block?
        %i[bush tree rock].include?(@type)
      end
    end
  end
end
