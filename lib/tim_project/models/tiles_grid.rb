# frozen_string_literal: true

module TimProject
  module Models
    class TilesGrid
      extend Forwardable

      def_delegators :@tiles, *(Array.     instance_methods(false) +
                                Enumerable.instance_methods(false))

      def initialize
        @tiles =
          Window::GRID.map do |column|
            column.map do |grid_tile_point|
              random_tile_type, random_tile_images =
                Assets::BACKGROUND_TILES.to_a.sample

              Tile.new(random_tile_images,
                       type: random_tile_type,
                       pos:  grid_tile_point)
            end
          end
      end

      # CP::Space#add_object use this method on the object passed in parameter
      def chipmunk_objects
        flat_map { |column| column.flat_map(&:chipmunk_objects) }
      end
    end
  end
end
