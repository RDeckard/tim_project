# frozen_string_literal: true

require 'gosu'
require 'chipmunk'

require 'singleton'
require 'forwardable'

require_relative 'assets'
require_relative 'window'
require_relative 'z_orders'
require_relative 'models/models'
require_relative 'scenes/scenes'

module TimProject
  FPS = 60
  class Game < Gosu::Window
    include Singleton
    include Models

    def initialize
      super Window::WIDTH, Window::HEIGHT
      self.caption = 'Tim'

      @scene = TimProject::Scenes::Demo.new
    end

    def update
      @scene.update
    end

    def draw
      @scene.draw
    end

    def button_down(id)
      close if id == Gosu::KB_ESCAPE

      super
    end
  end
end
