# frozen_string_literal: true

module TimProject
  module Window
    GRID_TILE_NUMBER_H = 20
    GRID_TILE_NUMBER_V = 15

    GRID_TILE_SIZE = Assets::TILE_SIZE * Assets::SCALE
    WIDTH   = GRID_TILE_SIZE * GRID_TILE_NUMBER_H
    HEIGHT  = GRID_TILE_SIZE * GRID_TILE_NUMBER_V

    GRID =
      (1..(WIDTH / GRID_TILE_SIZE)).map do |x|
        (1..(HEIGHT / GRID_TILE_SIZE)).map do |y|
          CP::Vec2.new(
            x * GRID_TILE_SIZE - GRID_TILE_SIZE / 2,
            y * GRID_TILE_SIZE - GRID_TILE_SIZE / 2
          )
        end
      end.freeze

    TOP_LEFT_GRID_POS     = GRID[0][0].freeze
    BOTTOM_RIGHT_GRID_POS = GRID[-1][-1].freeze
  end
end
