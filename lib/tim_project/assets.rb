# frozen_string_literal: true

module TimProject
  module Assets
    def self.load_tiles(image)
      Gosu::Image.load_tiles(image, TILE_SIZE, TILE_SIZE, retro: true)
    end

    TILE_SIZE = 16
    SCALE     = 4

    # Tiles
    Gosu::Image.new('assets/images/tileset.png', retro: true).
      subimage(0, 0, TILE_SIZE * 3, TILE_SIZE * 3).
      then(&method(:load_tiles)).
      then do |tiles|
        BACKGROUND_TILES =
          %i[
            lawn0 lawn1 lawn2
            grass0 grass1 grass2
            bush tree rock
          ].map.with_index { |sym, i| [sym, tiles[i].freeze] }.
            to_h.freeze
      end

    # Sprites
    Gosu::Image.new('assets/images/tim_animations.png', retro: true).
      then do |tim_sprites|
        TIM_SPRITES = {
          idle:       load_tiles(
            tim_sprites.subimage(TILE_SIZE * 0, 0, TILE_SIZE, TILE_SIZE * 4)
          ).freeze,
          walk_up:    load_tiles(
            tim_sprites.subimage(TILE_SIZE * 1, 0, TILE_SIZE, TILE_SIZE * 4)
          ).freeze,
          walk_down:  load_tiles(
            tim_sprites.subimage(TILE_SIZE * 2, 0, TILE_SIZE, TILE_SIZE * 4)
          ).freeze,
          walk_left:  load_tiles(
            tim_sprites.subimage(TILE_SIZE * 3, 0, TILE_SIZE, TILE_SIZE * 4)
          ).freeze,
          walk_right: load_tiles(
            tim_sprites.subimage(TILE_SIZE * 4, 0, TILE_SIZE, TILE_SIZE * 4)
          ).freeze
        }.freeze
      end
  end
end
