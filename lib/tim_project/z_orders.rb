# frozen_string_literal: true

module TimProject
  module ZOrders
    BACKGROUND, GROUND, LOW, MID, HIGH, SKY, HUD = *0..6
  end
end
