# frozen_string_literal: true

module TimProject
  module Scenes
    class Demo
      include Models

      def initialize
        @space = CP::Space.new
        @space.damping = 0

        @background = TilesGrid.new
        @space.add_objects(@background)
        @bounding_box =
          CP::BB.new(
            Window::TOP_LEFT_GRID_POS.x,
            Window::TOP_LEFT_GRID_POS.y,
            Window::BOTTOM_RIGHT_GRID_POS.x,
            Window::BOTTOM_RIGHT_GRID_POS.y
          )

        @tim = Tim.instance
        @space.add_object(@tim)

        @animated_tile =
          Tile.new(
            Assets::BACKGROUND_TILES.values,
            pos:    @tim.body.pos,
            zorder: ZOrders::LOW
          )
      end

      def update
        @tim.body.reset_forces

        @tim.go_up    if Gosu.button_down?(Gosu::KB_UP) ||
                         Gosu.button_down?(Gosu::GP_UP)
        @tim.go_down  if Gosu.button_down?(Gosu::KB_DOWN) ||
                         Gosu.button_down?(Gosu::GP_DOWN)
        @tim.go_left  if Gosu.button_down?(Gosu::KB_LEFT) ||
                         Gosu.button_down?(Gosu::GP_LEFT)
        @tim.go_right if Gosu.button_down?(Gosu::KB_RIGHT) ||
                         Gosu.button_down?(Gosu::GP_RIGHT)

        @space.step(1.0 / FPS)

        # Keep Tim in the boundaries
        @tim.body.pos = @bounding_box.clamp_vect(@tim.body.pos)
      end

      def draw
        @animated_tile.draw
        @background.each { |column| column.each(&:draw) }
        @tim.draw
      end
    end
  end
end
