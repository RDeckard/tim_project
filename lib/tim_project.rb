# frozen_string_literal: true

require_relative 'tim_project/game'

module TimProject
  def self.init
    Game.instance.show
  end
end

threads = []

if ARGV.first =~ /\A\+?(console|c)\z/
  require 'pry'
  include TimProject
  threads << Thread.new { Pry.start }
end

unless %w[console c].include? ARGV.first
  threads << Thread.new { TimProject.init }
end

threads.each(&:join)
